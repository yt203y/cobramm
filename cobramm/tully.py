#!/usr/bin/env python3
# coding=utf-8

#    COBRAMM
#    Copyright (c) 2019 ALMA MATER STUDIORUM - Università di Bologna

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.


from math import *
import numpy as np
import os
from os import system
import CBF
import random
import product
import shelve
import copy
import logwrt


def tully(command, geometry, state, cobcom, step):

    logwrt.writelog("\n")
    logwrt.startSubSection("TULLY'S FSSH")

    # read some general stuff we need from the shelve
    sef = shelve.open("cobram-sef", 'r')
    nroots = sef['nroots']
    AM = sef['AM']
    zeromat = sef['zeromat']
    try:
        SItime = sef['SItime']
    except KeyError:
        SItime = 0.0
    calc_coupl = sef['calc_coupl']
    sef.close()

    # generate all the empty matrices we need
    # HT: Hamiltonian at time t-dt (previous step)
    # H0: Hamiltonian at time t (present step)
    # DVT: TDNAC (NAC x vel) at time t-dt
    # DV0: TDNAC (NAC x vel) at time t
    HT = np.zeros(zeromat.shape, dtype=np.complex128)
    H0 = np.zeros(zeromat.shape, dtype=np.complex128)
    DV0 = np.zeros(zeromat.shape, dtype=np.complex128)
    DVT = np.zeros(zeromat.shape, dtype=np.complex128)

    # read specific things from the shelve
    sef = shelve.open("cobram-sef", 'r')
    # in case of Tully Hammes Schaefer hopping the TDNACs are computed directly
    # DVT and TDC == TDNAC(t) while DV0 and TDC_old == TDNAC(t-dt)
    # NACs are loaded for consistency but are not used
    DEarray = sef['DEarray']
    DE_oldarray = sef['DE_oldarray']
    NAC = sef['NAC']
    NAC_old = sef['NAC_old']
    if command[14] == '1':
        DVT = sef['TDC']
        DVT = np.array(DVT)
        DV0 = sef['TDC_old']
        DV0 = np.array(DV0)
    sef.close()

    newstate = int(state)
    if os.path.exists(os.getcwd() + '/SALTO') and str(os.popen('cat SALTO').read().strip()) == 'change state':
        system('rm SALTO')
    if command[14] != '1':
        # the energy difference between state and state+1 and state-1, respectively, is fetched from DEarray
        # by comparison to keyword/key ediff (86) the time step (short or long) is dtermined
        # same could be acchieved by checking the length of calc_coupl (?)
        # also with TDNACs it is possbile to have short and long steps!
        if state != 0:
            DE1 = DEarray[state][state - 1]
        else:
            DE1 = float(1000.0)
        if state < nroots-1:
            DE2 = DEarray[state][state + 1]
        else:
            DE2 = float(1000.0)
        if abs(DE1) < float(command[86]) or abs(DE2) < float(command[86]):
            logwrt.writelog("Time step is set to {0} in this step (SHORT)\n".format(command[84]))
            tstep = float(command[84]) * 41.341373337
            ttry = 'short'
        else:
            logwrt.writelog("Time step is set to {0} in this step (LONG)\n".format(command[83]))
            tstep = float(command[83]) * 41.341373337
            ttry = 'long'
        # the time step from last step t-dt is loaded as one needs SHORT time steps for two consequtive steps
        ttry_old = str(os.popen('cat TSTEP').read().strip())
        # the current time step is saved
        system('echo ' + str(ttry) + '>TSTEP')
        if abs(DE1) <= abs(DE2):
            comp_state = state - 1
        else:
            comp_state = state + 1
        ##in the special case when TDC (14==1) between S0 and S1 are not computed (90!=1) and there are higher states 
        ##comp_state should be set to state+1 in order to activate Tully
        ##when the dynamics is in S0, comp_state will be set to state + 1 (above) but as the TDC is 0.0 Tully will not be executed 
        #if command[14] == '1' and command[90] != 1 and state < nroots-1:
        #    comp_state = state + 1
    else:
        logwrt.writelog("Time step is set to {0} in this step\n".format(command[84]))
        tstep = float(command[83]) * 41.341373337
        ttry = 'short'
        system('echo ' + str(ttry) + '>TSTEP')

    # in order to evaluate Tully one needs NACs at two consequtive time steps (for interpolation)
    # at step t-2dt: the energy gap between two states goes below threshold set in ediff, NACs are assigned for comput
    # -> in tully ttry is set to 'short' and stored in TSTEP, ttry_old (from step t-3dt) is 'long'
    # at step t-dt : an empty NAC(t-2dt) is stored to NAC_old, the NACs are computed and stored in NAC(t-dt)
    # -> in tully ttry is set to 'short' and stored in TSTEP, ttry_old (from step t-2dt) is 'short'
    # at step t    : NAC(t-dt) are stored to NAC_old, the NACs are computed and stored in NAC(t)
    # -> in tully ttry is set to 'short' and stored in TSTEP, ttry_old (from step t-dt) is 'short'
    # ENTER Tully and compute hopping probability between t-dt and t
    TULLY = 'FALSE'
    # with TSH one can use longer time steps (i.e. both 'long' and 'short' should be set to the value of 'long')
    # in order to evaluate Tully one needs TDNACs at two consequtive time steps (for interpolation)
    # at step t-2dt: the energy gap between two states goes below threshold set in ediff, TDNACs are assigned for comput
    # at step t-dt : an empty TDNAC(t-2dt) is stored, TDNAC(t-dt) is computed from WF(t-2dt) and WF(t-dt) (at step 1 or
    # after hopping) or from WF(t-3dt), WF(t-2dt) and WF(t-dt)
    # at step t : TDNAC(t-dt) is stored as TDNAC_old,TDNAC(t) is computed is computed from WF(t-2dt), WF(t-dt) and WF(t)
    #        ENTER Tully and compute hopping probability between t-dt and t
    THS = 'FALSE'
    #if command[14] == '1' and abs(DVT[comp_state][state]) > 0.0 and abs(DV0[comp_state][state]) > 0.0 and \
            # ttry_old == 'short' and ttry == 'short':
    if command[14] == '1' and step > 1:
        THS = 'TRUE'
        logwrt.writelog("Solving the TDSE numerically\n")
    elif command[14] != '1' and NAC[comp_state][state] != [[], [], []] and \
            NAC_old[comp_state][state] != [[], [], []] and ttry_old == 'short' and ttry == 'short':
        TULLY = 'TRUE'
        logwrt.writelog("Solving the TDSE numerically\n")

    if TULLY == 'TRUE' or THS == 'TRUE':
        logwrt.writelog("\nEntering Tully's FSSH algorithm\n")

        # LOADING VELOCITIES
        # initialize empty velocity array at t-dt (xvel0, yvel0, zvel0) and t (xvel, yvel, zvel) ...
        xvel, yvel, zvel = [], [], []
        xvel0, yvel0, zvel0 = [], [], []
        # ... and load velocities from velocity.dat and velocityOLD.dat
        velinp = open('velocity.dat')
        vel = velinp.read().split('\n')
        velinp.close()
        velinp = open('velocityOLD.dat')
        vel0 = velinp.read().split('\n')
        velinp.close()

        # assign velocities of QM atoms
        for i in range(len(geometry.list_MEDIUM_HIGH)):
            if geometry.list_MEDIUM_HIGH[i] in geometry.list_HIGH:
                el = vel[i].split()
                xvel.append(float(el[0]))
                yvel.append(float(el[1]))
                zvel.append(float(el[2]))
                el = vel0[i].split()
                xvel0.append(float(el[0]))
                yvel0.append(float(el[1]))
                zvel0.append(float(el[2]))
        # assign velocities of atom-links (zeros!)
        for _ in range(geometry.NsubH):
                xvel.append(0.0)
                yvel.append(0.0)
                zvel.append(0.0)
                xvel0.append(0.0)
                yvel0.append(0.0)
                zvel0.append(0.0)
        xvel = np.array(xvel)
        yvel = np.array(yvel)
        zvel = np.array(zvel)
        xvel0 = np.array(xvel0)
        yvel0 = np.array(yvel0)
        zvel0 = np.array(zvel0)

        # for TULLY now check if the NACs change sign as the velocity will be rescaled along the NAC after hop
        # this assures continuity of the NACs which is required for interpolating between consequtive NACs
        # for THS the continuity condition is controlled with the sign of the diagonal elements of the ovlp matrix
        # between two consequtive time steps (performed in molcas.py)
        if command[14] != '1':
            # DETERMINE IF NACs CHANGED SIGN
            for i in range(len(NAC)):
                for j in range(len(NAC)):
                    NAC[i][j] = np.array(NAC[i][j])
                    NAC_old[i][j] = np.array(NAC_old[i][j])
            for z in range(len(calc_coupl)):
                state_i = calc_coupl[z][0]
                state_j = calc_coupl[z][1]
                # set the sign of the NAC(t) so that the angle with the NAC(t-dt) is < 90 deg
                # compute angle between the NAC(t-dt) and the NAC(t)
                angleDCp = product.angle(NAC[state_i][state_j][0], NAC[state_i][state_j][1], NAC[state_i][state_j][2],
                                         NAC_old[state_i][state_j][0], NAC_old[state_i][state_j][1],
                                         NAC_old[state_i][state_j][2])[0]
                logwrt.writelog("Angle between  NACs at this time step and at previous time step < " + str(
                    state_i + 1) + " |dR| " + str(state_j + 1) + " > is " + str(angleDCp) + "\n")
                # compute angle between the NAC(t-dt) and the -NAC(t)
                angleDCn = \
                    product.angle(-NAC[state_i][state_j][0], -NAC[state_i][state_j][1], -NAC[state_i][state_j][2],
                                  NAC_old[state_i][state_j][0], NAC_old[state_i][state_j][1],
                                  NAC_old[state_i][state_j][2])[
                        0]
                logwrt.writelog("Angle between -NACs at this time step and at previous time step < " + str(
                    state_i + 1) + " |dR| " + str(state_j + 1) + " > is " + str(angleDCn) + "\n")
                # if the angle between the NAC(t) and NAC(t-dt) is > 90, change the sign
                # note that CIrot, which was taking care of this by comparing the WFs is no longer needed
                if angleDCp > angleDCn:
                    if int(command[2]) > 0:
                        logwrt.writelog('The DC vector has changed sign! Sign correction applied!\n')
                    NAC[state_i][state_j] = np.array(NAC[state_i][state_j]) * float(-1.0)
                    NAC[state_j][state_i] = np.array(NAC[state_j][state_i]) * float(-1.0)

                # ANGLE BETWEEN NAC AND VELOCITY
                # compute angle between the NAC, selected above according to the energy gap
                # threshold, and the velocity at time step t
                resT = product.angle(NAC[state_i][state_j][0], NAC[state_i][state_j][1], NAC[state_i][state_j][2],
                                     xvel, yvel, zvel)
                if int(command[2]) > 0:
                    logwrt.writelog("Angle between NAC < " + str(state_i + 1) + " |dR| " + str(
                        state_j + 1) + " > and the velocity is " + str(resT[0]) + "\n")
                # compute angle between the NAC, selected above according to the energy gap threshold,
                # and the velocity at time step t-dt
                res0 = product.angle(NAC_old[state_i][state_j][0], NAC_old[state_i][state_j][1],
                                     NAC_old[state_i][state_j][2], xvel0, yvel0, zvel0)
                if int(command[2]) > 0:
                    logwrt.writelog("Angle between NAC < " + str(state_i + 1) + " |dR| " + str(
                        state_j + 1) + " > and the velocity at previous step is " + str(res0[0]) + "\n")
                angleT = resT[0]
                angle0 = res0[0]
                # stores 1 / "length of NAC vector"
                Nfactdc = resT[1]
                # store 1 / "length of velocity vector"
                Nfactvel = resT[2]
                # rap = (angle0 / 90.0) / ((180.0 - angleT) / 90.0)
                if angleDCp > angleDCn:
                    angleT = 180.0 - angleT

            # COMPUTE TIME-DERIVATIVE NACs (i.e. PROJECTIONS NAC x velocity) at
            # time t-dt (past step, H0) and t (present step, HT)
            for i in range(nroots):
                for j in range(nroots):
                    if i != j:
                        xdc = np.array(NAC[i][j][0])
                        ydc = np.array(NAC[i][j][1])
                        zdc = np.array(NAC[i][j][2])
                        xdc0 = np.array(NAC_old[i][j][0])
                        ydc0 = np.array(NAC_old[i][j][1])
                        zdc0 = np.array(NAC_old[i][j][2])
                        DVT[i][j] = np.add.reduce(xdc * xvel + ydc * yvel + zdc * zvel)
                        DV0[i][j] = np.add.reduce(xdc0 * xvel0 + ydc0 * yvel0 + zdc0 * zvel0)
            DVT = np.array(DVT)
            DV0 = np.array(DV0)

            # print the time-derivative couplings matrices
            logwrt.writelog("\nTime-derivative couplings at this step\n", 1)
            logwrt.writelog(logwrt.matrix_prettystring(DVT, ".8f"), 1)
            logwrt.writelog("\n", 1)
            logwrt.writelog("\nTime-derivative couplings at previous step\n", 1)
            logwrt.writelog(logwrt.matrix_prettystring(DV0, ".8f"), 1)
            logwrt.writelog("\n", 1)

        # following lines are executed by both Tully and THS
        # LOADING AMPLITUDES
        # initialize amplitudes every time we enter in Tully after a LONG time time step (?)
        if not list(AM):
            AM = list(AM)
            logwrt.writelog('Initializing Amplitudes\n')
            for i in range(nroots):
                AM.append(complex(0.0))
            AM[state] = complex(1.0)
            for i in range(nroots):
                logwrt.writelog("state {0} = {1:8.6f} + {2:8.6f}*i\n".format(i+1, AM[i].real, AM[i].imag))
            logwrt.writelog("\n")
            AM = np.array(AM)
            AM1 = copy.deepcopy(AM)
        else:
            # otherwise Amplitudes were loaded from the shelve
            AM1 = copy.deepcopy(AM)
        Atold = AM1
        At = AM

        # CREATE HAMILTONIAN AT TIME t-dt (previous step, H0) and t (present step, HT)
        for i in range(nroots):
            for j in range(i, nroots):
                if i == j:
                    HT[i][j] = complex(DEarray[0][i] / 627.51)
                    H0[i][j] = complex(DE_oldarray[0][i] / 627.51)
                else:
                    HT[i][j] = complex(DVT[i][j] * 1j)
                    H0[i][j] = complex(DV0[i][j] * 1j)
                    if command[14] != '1':
                        HT[j][i] = complex(-DVT[i][j] * 1j)
                        H0[j][i] = complex(-DV0[i][j] * 1j)
                    elif command[14] == '1':
                        HT[j][i] = complex(DVT[j][i] * 1j)
                        H0[j][i] = complex(DV0[j][i] * 1j)
        # the GS energy is set as reference 0.0
        HT[0][0] = complex(0.0)
        H0[0][0] = complex(0.0)

        logwrt.writelog("The Hamilton matrix at the present step\n", 1)
        logwrt.writelog(logwrt.matrix_prettystring(HT), 1)
        logwrt.writelog("\n", 1)
        logwrt.writelog("The Hamilton matrix at the previous step\n", 1)
        logwrt.writelog(logwrt.matrix_prettystring(H0), 1)
        logwrt.writelog("\n", 1)

        # set number of propagation cycles
        cycle = 100
        # set micro time step for electronic TDSE dt'
        dt = tstep / cycle
        # set Hamiltonian increment
        dH = (HT - H0) / cycle

        # CREATE AUXILIARY MATRICES
        b = copy.deepcopy(zeromat)
        g = copy.deepcopy(zeromat)
        P = copy.deepcopy(zeromat)
        He2 = np.zeros(zeromat.shape, dtype=np.complex128)
        one = copy.deepcopy(He2)
        for i in range(nroots):
            one[i][i] = complex(1.0)
        # zero matrix
        He2 = np.array(He2)
        HeOLD = He2
        # unit matrix
        one = np.array(one)

        # START PROPAGATION
        for i in range(cycle):
            Ht = (H0 + (.5 + i) * dH) * dt
            # BUILT UP EXPONENTIAL e^Ht THROUGH SERIES EXPANSION
            He1 = one
            He = one
            fact = complex(1.0)
            for lop in range(1, 100):
                for j in range(nroots):
                    for k in range(nroots):
                        He2[j][k] = 0.0
                        # compute the lop-1 power of Ht in every term of the expansion (Ht^(lop-1))
                        for l in range(nroots):
                            He2[j][k] = He2[j][k] + Ht[l][k] * He1[j][l]
                # compute the factorial (lop-1)!
                fact = fact * (1j / lop)
                # He1 contains the lop-1 power of Ht for use in the next term
                He1 = He2
                # obtain the term Ht^(lop-1)/(lop-1)!
                He2 = He2 * fact
                # add term to expansion
                He = He + He2
                # check for convergence
                summ = sqrt(abs(np.add.reduce(np.add.reduce((HeOLD - He) ** 2)).real))
                if summ < 1.0e-60:
                    break
                if lop == 99:
                    logwrt.fatalerror('no convergency in the unitary propagator')
                HeOLD = He
            # propagate the Amplitudes
            AAA = product.product(At, He)
            # Amplitudes at the end of the micro step t'+dt'
            At = np.array(AAA)
            # Amplitdues at the center of the micro step as the average between
            # the Amplitudes at micro time step t' and t'+dt'
            actA = (Atold + At) * .5

            # COMPUTE HOPPING PROBABILITY
            # interpolate between initial TDNAC(t-dt) and final TDNAC(t) to get TDNAC(t-dt+i*dt')
            DVtmp = DV0 + (DVT - DV0) * (i + .5) / cycle
            DVtmp = np.array(DVtmp)
            for k in range(nroots):
                for j in range(k, nroots):
                    b[k][j] = float(-2.0 * ((actA[k] * np.conjugate(actA[j])) * DVtmp[k][j]).real)
                    b[j][k] = float(-2.0 * ((actA[k] * np.conjugate(actA[j])) * -DVtmp[k][j]).real)
                    g[k][j] = g[k][j] + b[k][j] * dt
                    g[j][k] = g[j][k] + b[j][k] * dt
            Atold = copy.deepcopy(At)

        # STORE AND WRITE AMPLITUDES
        sef = shelve.open("cobram-sef")
        sef['AM'] = At
        sef.close()

        # store amplitudes for this step
        # this is actually no longer needed (?)
        out = open('Amplitudes.dat', 'w')
        logwrt.writelog("\nElectronic states amplitudes\n")
        for i in range(nroots):
            logwrt.writelog("state {0} = {1:8.6f} + {2:8.6f}*i\n".format(i+1, At[i].real, At[i].imag))
            out.write('%16.12f' % At[i].real + ' %16.12f ' % At[i].imag)
        logwrt.writelog("\n")
        out.close()

        SItime = SItime + tstep / 41.341373337
        if command[85] != '6':
            # append amplitudes from this step to list of amplitdues
            out = open('AmplitudesALL.dat', 'a')
            out.write('%10.4f' % SItime + '   '),
            for i in range(nroots):
                out.write('%16.12f' % At[i].real + ' %16.12f ' % At[i].imag + '   '),
            out.write('\n')
            out.close()

        # CALCULATE PROBABILITY
        # indices are reversed here to indicate hopping probability from state k to l
        for k in range(nroots):
            for j in range(nroots):
                if abs(At[k])**2 != 0:
                    P[k][j] = g[j][k] / abs(At[k])**2
                else:
                    P[k][j] = 0.0

        if command[80] != '0':
            # for testing purposes
            # a user defined value for the random number can be specified
            rnum = float(command[80])
            logwrt.writelog('User-defined "random" number (for testing purposes): {0:10.6f}\n'.format(rnum))
        else:
            # get a random number between 0 and 1
            rnum = float(random.randrange(0, 1000000, 1)) / 1000000
            logwrt.writelog('Random number selected for the hopping algorithm: {0:10.6f}\n'.format(rnum))

        # APPLY DECOHERENCE CORRECTION
        if command[85] == '6' or command[85] == '60':
            logwrt.writelog('\nUse decoherence correction from Granucci & Persico\n')
            At = product.decoherence(At, tstep, DEarray, state)
            logwrt.writelog("Electronic states amplitudes after correction\n")
            for i in range(nroots):
                logwrt.writelog("state {0} = {1:8.6f} + {2:8.6f}*i\n".format(i + 1, At[i].real, At[i].imag))
            logwrt.writelog("\n")
        hop = 'false'

        # MAKE DECISION FOR HOPPING
        # for HOPs from state 0:
        # to 1: rnum < g_01
        # to 2: g_01 < rnum < g_01 + g_02
        # to 3: g_01 + g_02 < rnum < g_01 + g_02 + g_03
        # for HOPs from state 1:
        # to 0: rnum < g_10
        # to 2: g_10 < rnum < g_10 + g_12
        # to 3: g_10 + g_12 < rnum < g_10 + g_12 + g_13
        # we need Psum and Psum_old
        Psum = 0
        Psum_old = 0
        for i in range(nroots):
            if i != state:
                Psum = Psum + P[state][i]
                if hop == 'false':
                    # hopping condition is evaluated here
                    if command[85] == '7':
                        if DEarray[state][i] < float(command[207]):   
                            logwrt.writelog('-----------------------------\n')
                            logwrt.writelog('  !!!!Gimme Hop Joanna!!!\n')
                            logwrt.writelog('-----------------------------\n')
                            system('echo change state >SALTO')
                            logwrt.writelog('hopping from state ' + str(state + 1) + ' to --> ' + str(i + 1) + "\n")
                            logwrt.writelog('hop ONLY based on energy difference\n')
                            logwrt.writelog('-----------------------------\n')
                            newstate = i
                            hop = 'true'
                            system('touch HOP')
                            if i > state and command[87] == '1' and command[14] != '1' and command[206] == '0':
                                condition = product.velscale(geometry, command, NAC[state][i][0], NAC[state][i][1],
                                                             NAC[state][i][2], xvel, yvel, zvel, DEarray[state][i],
                                                             tstep, cobcom)
                                if condition == 'true':
                                    newstate = i
                                else:
                                    logwrt.writelog("HOP rejected, not enough kinetic energy to along" +
                                                    " the DC(GD) vector to back-hop!\n")
                                    newstate = state
                                    hop = 'false'
                                    system('rm HOP')
                            elif i > state and command[87] == '0':
                                logwrt.writelog("HOP rejected (key 87)!\n")
                                newstate = state
                                hop = 'false'
                                system('rm HOP')
                    elif Psum_old < rnum < Psum:
                        logwrt.writelog('-----------------------------\n')
                        logwrt.writelog('  !!!!Gimme Hop Joanna!!!\n')
                        logwrt.writelog('-----------------------------\n')
                        system('echo change state >SALTO')
                        logwrt.writelog('hopping from state ' + str(state + 1) + ' to --> ' + str(i + 1) + "\n")
                        logwrt.writelog('-----------------------------\n')
                        newstate = i
                        hop = 'true'
                        system('touch HOP')
                        if i > state and command[87] == '1' and command[14] != '1' and command[206] == '0':
                            condition = product.velscale(geometry, command, NAC[state][i][0], NAC[state][i][1],
                                                         NAC[state][i][2], xvel, yvel, zvel, DEarray[state][i],
                                                         tstep, cobcom)
                            if condition == 'true':
                                newstate = i
                            else:
                                logwrt.writelog("HOP rejected, not enough kinetic energy to along" +
                                                " the DC(GD) vector to back-hop!\n")
                                newstate = state
                                hop = 'false'
                                system('rm HOP')
                        elif i > state and command[87] == '0':
                            logwrt.writelog("HOP rejected (key 87)!\n")
                            newstate = state
                            hop = 'false'
                            system('rm HOP')
                    else:
                        newstate = state
            if state != i:
                logwrt.writelog(' The Tully Hopping Probability to state ' + str(
                    i + 1) + ' is :  %10.6f ' % Psum_old + ' (total - current) %10.6f ' % P[state][
                                    i] + ' (current) %10.6f ' % Psum + ' (total)' + "\n")
            else:
                logwrt.writelog(' You are in this state ' + "\n")
            Psum_old = Psum

        logwrt.writelog("EXIT from Tully's FSSH algorithm\n\n")

    # if we do not enter into Tully
    # re-set the amplitudes every time Tully is not executed
    else:
        logwrt.writelog("Nothing to be done in Tully FSSH, resetting Amplitudes\n")
        AM = []

        # OW 7/18 print populations and occupations even when tully is inactive

        occup = []
        popul = []

        for i in range(nroots):
            AM.append(complex(0.0))
            occup.append(float(0.0))
            popul.append(float(0.0))
        AM[state] = complex(1.0)
        occup[state] = float(1.0)
        popul[state] = float(1.0)
        sef = shelve.open("cobram-sef")

        # OW introduce stopping criterion

        if occup[0] == 1 and float(command[122]) > 0:
            stop = float(sef['stop'])
            stop = stop + tstep / float('41.341373337')
            if float(command[122]) > 0 and (stop >= float(command[122])):
                sef.close()
                logwrt.writelog('The trajectory has been in the ground state for '
                                + str(command[122]) + ' fs, calculation stopped')
                if not logwrt.DEBUG_COBRAMM_RUN:  CBF.garbager(geometry, command)
                logwrt.cobramend()
            else:
                logwrt.writelog('the trajectory is in S0 for ' + str(stop) +
                                ' fs, continuing until we reach ' + str(command[122]) + ' fs')
                sef['stop'] = stop
        else:
            sef['stop'] = '0.0'

        logwrt.writelog("Electronic states amplitudes\n")
        for i in range(nroots):
            logwrt.writelog("state {0} = {1:8.6f} + {2:8.6f}*i\n".format(i + 1, AM[i].real, AM[i].imag))
        logwrt.writelog("\n")

        AM = np.array(AM)
        AM1 = copy.deepcopy(AM)

        sef['AM'] = AM
        sef['AM1'] = AM1
        sef.close()

        with open('AmplitudesALL.dat', 'a') as out:
            out.write('%10.4f' % SItime + '   '),
            for i in range(nroots):
                out.write('%16.12f' % AM[i].real + ' %16.12f ' % AM[i].imag + '   '),
            out.write('\n')

    # NACs and TDNACs are stored in the shelve if not zero
    sef = shelve.open("cobram-sef")
    NAC_old = copy.deepcopy(NAC)
    sef['NAC_old'] = NAC_old
    sef['newstate'] = newstate
    sef['SItime'] = SItime
    if command[14] == '1':
        TDC_old = copy.deepcopy(DVT)
        sef['TDC_old'] = TDC_old
    sef.close()

    return int(newstate)
