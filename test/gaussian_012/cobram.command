!keyword
type=ts 
nsteps=100
qm-type=gauss
qmem=200MB
geomem=300MB
?keyword

!gaussian
#p td(nstates=1,root=1) sto-3g b3lyp nosym 

Comment line

0 1 
?gaussian

!ts
#p opt(ts,NewEstmFC,noeigentest,nolinear,nomicro,maxstep=30) iop(1/9=1,5/6=6)
?ts

