!command
60 3         number of steps
1 freqxg     calculation type mdv for dynamic, or optxg for geometry optimization
51 6         QM program 1 gaussian, 6 molcas, 7 molpro
53 180       Memory (in mega words for molpro, MB for molcas)
194 1        use cartesians functions in molcas seward
196 1        use RI
197 STO-3G   Basis set in molcas
?command

!molcas
&RASSCF  &END
Symmetry
1
Spin
1
nActEl
6 0 0
CIroot
1 1
1
Inactive
6
RAS2
6
RLXR
1
LumOrb
End of Input

&ALASKA &END
Show
End of input

?molcas
