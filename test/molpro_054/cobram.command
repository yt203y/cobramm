!keyword
type=mdv qm-type=molpro nsteps=5 qmem=180MW tstep=0.25 surhop=cirot ediff=30.0 basis=STO-3G
?keyword

!sander
MD Pentadien
 &cntrl
  imin   = 1,
  maxcyc = 0,
  ntb    = 0,
  igb    = 0,
  ntr    = 0,
  ibelly = 1,
  cut    = 9
 /
?sander

!molpro
{multi;
occ,40;
closed,38;
wf,78,1,0;
state,3;
Weight,1,1,0;
CPMCSCF,GRAD,2.1,record=5100.1;}
?molpro

!rattle
39 40
39 41
40 41
42 43
42 44
43 44
?rattle

