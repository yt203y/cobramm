!command
60 3        number of steps
1 optxg     calculation type mdv for dynamic, or optxg for geometry optimization
51 6        QM program 1 gaussian, 6 molcas, 7 molpro
53 1000     Memory (in mega words for molpro, MB for molcas)
194 1       use cartesians in molcas seward
196 0       use RI (if 1)
197 STO-3G  Basis set in molcas
?command

!rfield

RF-input
PCM-model
solvent
  water
DIELectric  constant
  78.39
CONDuctor  version
AARE
  0.4
End  of  rf-input 

?rfield


!molcas
&SCF     &END
Occupied
22
End of Input

&RASSCF  &END
Symmetry
1
Spin
1
nActEl
6 0 0
CIroot
1 1
1
Inactive
19
RAS2
6
Iter
100 100
RLXR
1
LumOrb
*NONEquilibrium
End of Input

&ALASKA &END
Show
End of input

?molcas


