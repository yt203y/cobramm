!command
60 2        number of steps       
1 mdv       calculation type mdv for dynamic, or optxg for geometry optimization
51 6        QM program 1 gaussian, 6 molcas, 7 molpro
53 1000     Memory (in mega words for molpro, MB for molcas)
83 0.75     timestep (fs) 
84 0.25     timestep close to the CI (fs)
85 1        use CI-vector rotation 
86 10.0     energy gap to activate smaller timestep and CI surf hopping
197 STO-3G  Basis set in molcas
?command

!sander 
MD Pentadien
 &cntrl
  imin   = 1,
  maxcyc = 0,
  ntb    = 0,
  igb    = 0,
  ntr    = 0,
  ibelly = 1,
  cut    = 9
 /
?sander 


!molcas
 &SCF     &END
Occupied
 43
End of Input

 &RASSCF  &END
Symmetry
 1
Spin
 1
nActEl
 6 0 0
CIroot
 2 2
 1 2
 1 1
Inactive
40
RAS2
 6
RLXR
 2
LumOrb
End of Input

 &ALASKA &END
Show
End of input

 &RASSI &END
NR OF JOBIPHS
  1 2
  1
  2
CIPRint
THRS
0.0
End of Input

?molcas

