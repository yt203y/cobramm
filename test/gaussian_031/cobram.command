!keyword
type=optxg 
nsteps=1000  
qm-type=gauss
qmem=200MB
geomem=300MB
stepsize=1
savQMlog=5
?keyword


!sander
comment line
&cntrl
imin   = 1,
maxcyc = 0,
ntb    = 0,
igb    = 0,
ntr    = 0,
ibelly = 1,
cut    = 10
/
?sander

!optxg
#P opt=(cartesian)
?optxg

!gaussian
#p hf sto-3g nosym

Comment line

0 1 
?gaussian


