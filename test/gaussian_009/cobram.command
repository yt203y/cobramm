!keyword
type=optxg 
nsteps=10  
qm-type=gauss
qmem=200MB
geomem=300MB
basis=STO-3G
savwfu=3
savQMlog=3
?keyword

!gaussian
#p casscf(2,2,nroot=1) nosym scf(conver=5)

Comment line

+1 1 
?gaussian

!redundant 
1 2 F
?redundant

!optxg
#p opt(noeigentest,nolinear,nomicro,addredundant,maxstep=30)
?optxg

