!keyword
type=optxg 
nsteps=3  
qm-type=gauss
qmem=200MB
geomem=300MB
?keyword

!gaussian
#p td(nstates=3,root=2) gen b3lyp nosym 

Comment line

+1 1 
?gaussian

!gen
H
STO-3G
****
C
6-31G(d,p)
****
N
6-31G(d',p')
****
?gen

