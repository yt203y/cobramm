!keyword
type=freqxg
nsteps=1000  
qm-type=gauss
qmem=200MB
geomem=300MB
nproc=1
?keyword

!gaussian
#p casscf(2,2,nroot=2,stateaverage)/sto-3g nosym scf(conver=5)

Comment line

0 1 
?gaussian

!gaussweights
0.50000000 0.50000000
?gaussweights

