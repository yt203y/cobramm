!keyword
type=optxg 
nsteps=3  
qm-type=gauss
qmem=200MB
geomem=300MB
?keyword


!sander
comment line
&cntrl
imin   = 1,
maxcyc = 0,
ntb    = 0,
igb    = 0,
ntr    = 0,
ibelly = 1,
cut    = 10
/
?sander


!gaussian
#p casscf(2,2,nroot=2,stateaverage)/6-31G nosym scf(conver=5)

Comment line

0 1 
?gaussian

!gaussweights
0.50000000 0.50000000
?gaussweights


