import unittest
import os, shutil, sys
import subprocess
import re
import numpy as np

# define directory testRootDir = where files are stored
testRootDir = os.path.dirname(os.path.abspath(__file__))
testRunDir  = os.path.join(testRootDir,"test_run")

# import python file with the definition of COBRAMM run command
parentdir = os.path.dirname(testRootDir)
if not parentdir in sys.path: 
    sys.path.insert(0,parentdir) 
import test_config

# define directory testRunDir = where calculation is run 
testTmpDir  = os.path.join(test_config.tmpPath, "test_cobramm_run" )

# Lists that define the input files that need to be copied to run the test calculation
inputFileList  = ["cobram.command", "real_layers.xyz" ]

# name of the reference and test files
logTest = os.path.join(testRunDir,"cobramm.log")
logRef  = os.path.join(testRootDir,"cobramm.log_ref")
outTest = os.path.join(testRunDir,"cobramm.xml")
outRef  = os.path.join(testRootDir,"cobramm.xml_ref")
geomTest = os.path.join(testRunDir,"geometry.log")
geomRef  = os.path.join(testRootDir,"geometry.log_ref")

class test(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):

        # When overwrite option is used, first remove everything from previous test run
        if test_config.overWrite:
            if os.path.isdir(testRunDir): shutil.rmtree(testRunDir)
        # and clean temporary directory
        if os.path.isdir(testTmpDir):
            shutil.rmtree(testTmpDir)
        
        # run the test calculation only when the testRunDir directory is not present
        if not os.path.isdir(testRunDir):
            # create testRunDir directory and move there
            os.mkdir(testTmpDir); os.chdir(testTmpDir)
            # copy input file to the run directory
            for f in inputFileList:
                shutil.copyfile(os.path.join(testRootDir,f), os.path.join(testTmpDir,f))
            # run COBRAMM
            stdoutf = open("cobramm.log","w")
            subprocess.call(test_config.cobrammCommand, stderr=subprocess.STDOUT, stdout=stdoutf)
            stdoutf.close()
            # copy final output files to the test directory
            shutil.copytree(testTmpDir, testRunDir)

    @classmethod
    def tearDownClass(cls):
        # remove the directory in which the test calculation has been run
        if os.path.isdir(testTmpDir):
            shutil.rmtree(testTmpDir)

    def test_trajEnergy1(self):
        # extract the IRC geometries the function in test_config
        traj_Test = test_config.extract_QMMMEnergy_logfile( logTest, 1 )
        traj_Ref  = test_config.extract_QMMMEnergy_logfile( logRef,  1 )
        # sum up the difference between the energies step by step
        self.assertLess( test_config.traj_energy_difference(traj_Test, traj_Ref), test_config.traj_energy_threshold )

    def test_trajEnergy2(self):
        # extract the IRC geometries the function in test_config
        traj_Test = test_config.extract_QMMMEnergy_logfile( logTest, 2 )
        traj_Ref  = test_config.extract_QMMMEnergy_logfile( logRef,  2 )
        # sum up the difference between the energies step by step
        self.assertLess( test_config.traj_energy_difference(traj_Test, traj_Ref), test_config.traj_energy_threshold )


if __name__ == '__main__':
    unittest.main(verbosity=2)

