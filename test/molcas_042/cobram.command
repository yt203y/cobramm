!command
60 10       number of steps       
1 mdv       calculation type mdv for dynamic, or optxg for geometry optimization
51 6        QM program 1 gaussian, 6 molcas, 7 molpro
53 180      Memory (in mega words for molpro, MB for molcas)
83 0.75     timestep (fs) 
84 0.25     timestep close to the CI (fs)
85 1        use CI-vector rotation 
86 10.0     energy gap to activate smaller timestep and CI surf hopping
93 5        switch off weighting abruptly.  n=0: off, n>0 switch off at dE=n kcal/mol
197 STO-3G  Basis set in molcas
200 5       use singlepoint input every 5 steps
100 5       save wavefunction every 5 steps
99  2       save molcas log file every 2 steps
?command

!molcas
 &SCF     &END
Occupied
 22
End of Input

 &RASSCF  &END
Symmetry
 1
Spin
 1
nActEl
 6 0 0
CIroot
 2 2
 1 2
 1 1
Inactive
19
RAS2
 6
RLXR
 2
LumOrb
End of Input


 &ALASKA &END
Show
End of input

 &RASSI &END
NR OF JOBIPHS
  1 2
  1
  2
CIPRint
THRS
0.0
End of Input

?molcas

!singlepoint

&RASSCF  &END
Symmetry
1
Spin
1
nActEl
6 0 0
CIroot
3 3
1 2 3
1 1 1
Inactive
19
RAS2
6
LumOrb
End of Input

?singlepoint



