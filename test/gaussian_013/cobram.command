!keyword
type=ts 
nsteps=100
qm-type=gauss
qmem=200MB
geomem=300MB
?keyword

!gaussian
#p casscf(2,2,nroot=2,stateaverage)/sto-3g nosym scf(conver=5)

Comment line

+1 1 
?gaussian

!gaussweights
0.50000000 0.50000000
?gaussweights

!redundant 
1 2 F
?redundant

!ts
#p opt(ts,NewEstmFC,noeigentest,nolinear,nomicro,addredundant,maxstep=30) iop(1/9=1,5/6=6)
?ts

