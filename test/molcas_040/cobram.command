!command
60 2     number of steps
1 cip    calculation type mdv for dynamic, or optxg for geometry optimization
9 4
13 3
51 6        QM program 1 gaussian, 6 molcas, 7 molpro
53 1000     Memory (in mega words for molpro, MB for Molcas)
194 1       use cartesian functions in molcas seward
196 1       use RI
197 STO-3G  Basis set in molcas
?command

!sander
MD Pentadien
&cntrl
imin   = 1,
maxcyc = 0,
ntb    = 0,
igb    = 0,
ntr    = 0,
ibelly = 1,
cut    = 9
/
?sander 

!molcas
&RASSCF  &END
Symmetry
1
Spin
1
nActEl
6 0 0
CIroot
3 3 1
Inactive
19
RAS2
6
LumOrb
End of Input

&caspt2
imag=0.2
ipea=0.0
maxiter=200
multistate=3 1 2 3 
PROP

?molcas


