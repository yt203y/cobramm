!keyword
type=irc
nsteps=20
ircpoints=2  
ircstepsize=10
qm-type=gauss
qmem=200MB
geomem=300MB
conver=1000
sstep=-1
?keyword

!gaussian
#p casscf(2,2,nroot=2,stateaverage)/sto-3g nosym scf(conver=5) 

Comment line

+1 1 
?gaussian

!gaussweights
0.50000000 0.50000000
?gaussweights

