!keyword
type=sp
qm-type=gauss
qmem=200MB
savwfu=1
?keyword


!sander
comment line
&cntrl
imin   = 1,
maxcyc = 0,
ntb    = 0,
igb    = 0,
ntr    = 0,
ibelly = 1,
cut    = 10
/
?sander


!gaussian
#p casscf(2,2,nroot=3,stateaverage)/gen nosym scf(conver=5)

Comment line

0 1 
?gaussian


!gaussweights
0.33333333 0.33333333 0.33333333
?gaussweights


!gen
H
STO-3G
****
C,O
6-31G(d,p)
****
N
6-31G(d',p')
****
1 0
SP   1 1.00
 0.4380000000D-01  0.1000000000D+01  0.1000000000D+01
****
?gen


