!command
60  sp        number of steps
1   optxg     calculation type mdv for dynamic, or optxg for geometry optimization
51  6         QM program 1 gaussian, 6 molcas, 7 molpro
53  4000MB    Memory in mega words
196 1         RICD
197 STO-3G    Basis set in molcas
?command

!sander 
MD Pentadien
 &cntrl
  imin   = 1,
  maxcyc = 0,
  ntb    = 0,
  igb    = 0,
  ntr    = 0,
  ibelly = 1,
  cut    = 9
 /
?sander 

!molcas
 &SCF     &END
Occupied
 43
End of Input

 &RASSCF  &END
Symmetry
 1
Spin
 1
nActEl
 6 0 0
CIroot
 2 2
 1 2
 1 1
Inactive
40
RAS2
 6
RLXR
 2
LumOrb
End of Input

?molcas

