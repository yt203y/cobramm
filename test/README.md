
# COBRAMM TEST SUITE
          
the directory contains a collection of test cases that are 
intended to be used to test the normal execution of COBRAMM.

To test the correct installation of the code, please use
the run\_tests script that is located in the root directory
of COBRAMM:

    cd $COBRAM_PATH  
    ./run_tests

The following is the list of test cases. 
Each test is located in a subdirectory with name
                 `QM_CODE`_`TEST_#` 


 |  QM CODE   | TEST # |     DESCRIPTION                         |          TEST                          |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Gaussian  | 001    | Hartree-Fock single point, no MM        | SP energies (GS)                       |
 |  Gaussian  | 002    | DFT B3LYP single point, no MM           | SP energies (GS)                       |
 |  Gaussian  | 003    | CIS single point, no MM                 | SP energies (GS + 5 ES)                |
 |  Gaussian  | 004    | SS-CASSCF single point, no MM           | SP energies (GS)                       |
 |  Gaussian  | 005    | SA-CASSCF single point, no MM           | SP energies (GS + 1 ES)                |
 |  Gaussian  | 006    | TD-DFT single point, no MM              | SP energies (GS + 5 ES)                |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Gaussian  | 007    | Hartree-Fock optimization, no MM        | opt geometry and energy (GS)           |
 |  Gaussian  | 008    | DFT B3LYP optimization, no MM           | opt geometry and energy (GS)           |
 |  Gaussian  | 009    | SS-CASSCF optimization, no MM           | opt geometry and energy (GS)           |
 |  Gaussian  | 010    | SA-CASSCF optimization, no MM           | opt geometry and energy (GS + 1 ES)    |
 |  Gaussian  | 011    | TD-DFT optimization, no MM              | opt geometry and energy (GS + 3 ES)    |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Gaussian  | 012    | TD-DFT TS opt (NewEstmFC), no MM        | opt geometry and energy (GS + 2 ES)    |
 |  Gaussian  | 013    | SA-CASSCF TS opt (NewEstmFC), no MM     | opt geometry and energy (GS + 1 ES)    |
 |  Gaussian  | 014    | SA-CASSCF TS opt (RCFC), no MM (\*)     | opt geometry and energy (GS + 1 ES)    |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Gaussian  | 015    | TD-DFT IRC, no MM                       | IRC geometry and energy                |
 |  Gaussian  | 016    | SA-CASSCF IRC, no MM                    | IRC geometry and energy                |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Gaussian  | 017    | HF frequency (serial), no MM            | frequencies                            |
 |  Gaussian  | 018    | DFT B3LYP frequency (serial), no MM     | frequencies                            |
 |  Gaussian  | 019    | TD-DFT frequency (serial), no MM        | frequencies                            |
 |  Gaussian  | 020    | SA-CASSCF frequency (serial), no MM     | frequencies                            |
 |  Gaussian  | 021    | HF frequency (parallel), no MM          | frequencies                            |
 |  Gaussian  | 022    | DFT B3LYP frequency (parallel), no MM   | frequencies                            |
 |  Gaussian  | 023    | TD-DFT frequency (parallel), no MM      | frequencies                            |
 |  Gaussian  | 024    | SA-CASSCF frequency (parallel), no MM   | frequencies                            |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Gaussian  | 025    | TD-DFT molecular dynamics, no MM        | energies of the snapshots of the traj  |
 |  Gaussian  | 026    | SA-CASSCF molecular dynamics, no MM     | energies of the snapshots of the traj  |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Gaussian  | 027    | HF single point, with MM                | SP energies (GS)                       |
 |  Gaussian  | 028    | TD-DFT single point, with MM            | SP energies (GS + 2 ES)                |
 |  Gaussian  | 029    | SS-CASSCF single point, with MM         | SP energies (GS)                       |
 |  Gaussian  | 030    | SA-CASSCF single point, with MM         | SP energies (GS + 2 ES )               |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Gaussian  | 031    | HF optimization, with MM                | opt geometry and energy (GS)           |
 |  Gaussian  | 032    | DFT optimization, with MM               | opt geometry and energy (GS)           |
 |  Gaussian  | 033    | TD-DFT optimization,  with MM           | opt geometry and energy (GS + 2 ES)    |
 |  Gaussian  | 034    | SA-CASSCF optimization, with MM         | opt geometry and energy (GS + 1 ES)    |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Gaussian  | 035    | SA-CASSCF TS opt (NewEstmFC), with MM   | opt geometry and energy (GS + 1 ES)    |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Gaussian  | 036    | TD-DFT frequency (parallel), with MM    | frequencies                            |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Gaussian  | 037    | TD-DFT molecular dynamics, with MM      | energies of the snapshots of the traj  |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Molcas    | 038    | MCSCF optimization, no MM               | opt geometry and energy (GS)           |
 |  Molcas    | 039    | MCSCF optimization with PCM, no MM      | opt geometry and energy (GS)           |
 |  Molcas    | 040    | MCSCF conical intersection, no MM (\*)  | opt geometry and energy (GS + 2 ES)    |
 |  Molcas    | 041    | MCSCF frequency, no MM                  | frequencies                            |
 |  Molcas    | 042    | MCSCF molecular dynamics, no MM         | energies of the snapshots of the traj  |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Molcas    | 043    | MCSCF molecular dynamics, with MM       | energies of the snapshots of the traj  |
 |  Molcas    | 044    | MCSCF MD, with MM (ghost atoms)         | energies of the snapshots of the traj  |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Molpro    | 045    | Hartree-Fock optimization, no MM        | opt geometry and energy (GS)           |
 |  Molpro    | 046    | MCSCF optimization, no MM               | opt geometry and energy (GS + 2 ES )   |
 |  Molpro    | 047    | MCSCF conical intersection, no MM       | opt geometry and energy (GS + 2 ES )   |
 |  Molpro    | 048    | MCSCF IRC, no MM                        | IRC geometry and energy (GS + 2 ES )   |
 |  Molpro    | 049    | MCSCF Frequency, no MM                  | frequencies                            |
 |  Molpro    | 050    | MCSCF molecular dynamics, no MM         | energies of the snapshots of the traj  |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Molpro    | 051    | MP2 optimization, with MM               | opt geometry and energy (GS)           |
 |  Molpro    | 052    | MCSCF conical intersection,with MM (\*) | opt geometry and energy (GS + 3 ES )   |
 |  Molpro    | 053    | MCSCF IRC, with MM (\*)                 | opt geometry and energy (GS + 3 ES )   |
 |  Molpro    | 054    | MCSCF MD (CI ovlp), with MM             | energies of the snapshots of the traj  |
 |  Molpro    | 055    | MCSCF MD (FSSH + Persico), with MM      | energies of the snapshots of the traj  |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Molcas    | 056    | MCSCF frequency, no MM, parallel run    | frequencies                            |
 |  Molcas    | 057    | MCSCF optimization, no MM, parallel run | opt geometry and energy (GS)           |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Gaussian  | 058    | TD-DFT opt, no MM, numer deriv, + displ | opt geometry, all energies             |
 |  Gaussian  | 059    | TD-DFT opt, no MM, numer deriv, +/- dis | opt geometry, all energies             |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Molcas    | 060    | single point QM/MM calculation          | SP energy (GS + 1 ES)                  |
 |  Molcas    | 061    | MCSCF surface hopping, with MM          | energies of the snapshots of the traj  |
 |  Molcas    | 062    | CASPT2 surface hopping, no MM           | energies along the traj (GS + 1 ES)    |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Gaussian  | 063    | TDDFT surface hopping, no MM (\*\*)     | energies along the traj (GS + 4 ES)    |
 |  Gaussian  | 064    | TDDFT hop to GS, no MM (\*\*)           | energies along the traj (GS + ES)      |
 |------------|--------|-----------------------------------------|----------------------------------------|
 |  Molcas    | 065    | MS-CASPT2 surface hopping (TDC), with MM| energies along the traj (GS + 1 ES)    | 
 |  Molcas    | 066    | SS-CASPT2 surface hopping (TDC), with MM| energies along the traj (GS + 1 ES)    |
 |  Molcas    | 067    | MS-CASPT2 geometry optimization, with MM| opt geometry and energy (GS + 4 ES)    | 
 |  Molcas    | 068    | SS-CASPT2 geometry optimization, with MM| opt geometry and energy (GS + 4 ES)    |
 |------------|--------|-----------------------------------------|----------------------------------------|
    
     (\*) The results of the test differ significantly depending on the version of Gaussian.
          In the test suite run, the calculation is automatically tested against an output
          obtained with Gaussian 16. For users that need to check Gaussian 09 results,
          the output for Gaussian 09 can be found in the test directory.

   (\*\*) Works only with Gaussian 09 (TDA option is required to compute WF overlap with TD-DFT)

