!keyword
type=optxgp
numproc=3
qm-type=molcas
qmem=1000MB
savwfu=0
savQMlog=1
nsteps=5
basis=STO-3G
numrlx=2
ricd=1
?keyword

!command
204 2.5
205 1
?command

!sander
MD Pentadien
&cntrl
imin   = 1,
maxcyc = 0,
ntb    = 0,
igb    = 0,
ntr    = 0,
ibelly = 1,
cut    = 9
/
?sander

!molcas
&RASSCF
Symm=1
Spin=1
Nactel=14 0 0
CIRoot=5 5 1
Inacti=22
Ras1=0
Ras2=10
Ras3=0
Lumorb
Thrs=1.0D-07 1.0D-02 1.0D-02
CIMX=200
OrbListing
NOTHING
PRINT
6 2 1 1 1 1 1 2 1

&caspt2
imag=0.2
ipea=0.0
maxiter=200
multistate=5 1 2 3 4 5 
NOMULT
PROP
?molcas

