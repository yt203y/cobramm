!command
60 3        number of steps
1 optxg     calculation type mdv for dynamic, or optxg for geometry optimization
51 6        QM program 1 gaussian, 6 molcas, 7 molpro
53 1000     Memory (in mega words for molpro, MB for Molcas)
194 1       use cartesian functions in molcas seward
196 1       use RI
197 STO-3G  Basis set in molcas
100 1
?command

!molcas
&RASSCF  &END
Symmetry
1
Spin
1
nActEl
6 0 0
CIroot
1 1
1
Inactive
19
RAS2
6
Iter
200 200
RLXR
1
LumOrb
End of Input

&ALASKA &END
Show
End of input

&RASSI &END
NR OF JOBIPHS
1 1
1
CIPRint
THRS
0.0
End of Input

?molcas

!optxg
#P OPT=nomicro
?optxg

