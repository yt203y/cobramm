!keyword
type=mdvp
numproc=3
qm-type=molcas
qmem=500MB
savwfu=0
savQMlog=1
nsteps=10
tstep=0.1
tsshort=0.1
surhop=persico
ediff=1000.0
backhop=off
nacs=tdc
basis=STO-3G
rand=0.0001
?keyword

!molcas
&RASSCF
  SYMMETRY= 1
  SPIN= 1
  NACTEL= 8 0 0
  CIROOT= 2 2; 1 2; 1 1
  INACTIVE= 2
  RAS2= 8
  RLXR= 2
  LUMORB

&CASPT2
  IMAG=0.2
  IPEA=0.0
  MAXITER=200
  MULTISTATE=2 1 2
  PROP

?molcas

