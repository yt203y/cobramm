COBRAM-beta_5.7_devel6 supports now keyword driven input. In the
cobram.command, keywords instead of numeric commands can be specified
in the !keywords section.

Example:

!keywords
nsteps=1000
numrlx=1
type=mdvp
qm-type=molcas
qmem=180MB
tstep=0.25
tsshort=0.1
surhop=persico
ediff=30.0
npfreq=8
?keywords


will start a parallel MD (8 processors, 180MB memory) with 1000 steps, relaxing root
1 with molcas numerically, using Tully surface hopping and Persico's decoherence
correction. The threshold for selective Tully surface hopping is 30
kcal/mol, with time steps 0.1 fs reaching this value.

Note that numeric commands can still be given in the !command section,
the !command values will however always overwrite the !keywords!

Allowed keywords and values:


Common keywords
================

type : select which action to perform; mdv, mdvp, optxg, opxgp, freqxg, freqxgp
------

qm-type : program to use for QM calculation: none, gauss, orca, dftb, dftb+, turbo, molcas, molpro, dft-mrci
-------- 

mm-type : currently only amber is supported
---------

qmem : memory for QM calculation (e.g. 1000MB)
------

geomem :  memory for gaussian geometry optimization, e.g. 100MB
--------

amber : how to compute velocties for amber: serial, cuda, mpi, velo
-------

nproc : number of processors 
-------

basis : specify basis set
-------

cart : use cartesian basis functions
------

nsteps : number of steps for MD or geometry optimization
--------

tstep : timestep for MD in fs
-------

tsshort : short time step in coupling region
---------

surhop : type of surface hopping algorithm : none, cirot, charge, cich, massey, tully, persico
--------

ediff : energy difference for selective Tully algorithm in kcal/mol
-------

savwfu : save wavefunction every X steps
--------

egrad : use egrad for turbomole : yes / no
-------

rigrad : use rigrad for turbomole : yes / no
--------

ricd : compute using RICD in molcas 
------

single : perform single point computation given in !singlepoint  
--------

sstep : perform !singlepoint every X steps
-------




more special keywords
=====================

bomb :  if '1' do not stop the calculation if wavefunction is not converged (careful!)  
------

npfreq : number of processors for numerical gradients
--------

q1cor : correct gradient of q1 : yes / no
-------

verbose : keep verbose output : yes / no 
---------

debug : print debugging info : yes / no 
-------

freeze : specify which region to freeze in old g03 optimizer: no, medium, high, active
--------

border : freeze border atoms if yes
--------

distype : type of displacement in numerical calculations: 0: only +,
--------- 1: +/-, 2: only when hopping is not active, +/- when hopping os active

cicopt : 0: optimize only CI coefficients during numerical compuation of the gradient 
-------- and NACs; 1: optimize MO and CI coeffcients (works only with MOLCAS so far)

displace : displacement for numerical calc. in Angstrom
----------

numrlx : root to relax in numerical gradient computations
--------

nacs : compute numerical NACS in molcas : yes / no
------

brute : brute switch from SA to state specific casscf, give threshold in kcal/mol
-------

delwfu : delete wfu every X steps
--------

charges : use charges from input for the QM calculation : yes / no
---------

pt2roots : number of roots for caspt2 computation
----------

ghost : use ghost atoms in molcas (better don't) : yes / no
-------

scale-iso : scale initial velocities for isotopes given in !isotope section
-----------

cdth : change cholesky threshold (careful with this as well!)
------

