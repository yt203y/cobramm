Amber Interface

Starting from version 5.7, only Amber versions above Amber10 are
supported, i.e. the old versions 8 and 9 will no longer work, since
many commands and data handling has significantly changed. If you want
to use Amber < 9 you will have to step back to cobram version
5.6. 'Usually' (see below), Amber 8/9 created real.top and real.crd
files will also work with newer versions of Amber.

The standard Amber input reads 

!sander
Comment line
&cntrl
imin   = 1,
maxcyc = 0,
ntb    = 0,
igb    = 0,
ntr    = 0,
ibelly = 1,
cut    = 12 
/
?sander

It performs a single point energy-only computation of the whole system as well
as of the High layer only. This is necessary for the substraction scheme
COBRAMM uses for computing the QM/MM energy. The computations are also used to
obtain the MM-part of the gradients of High and Medium layers. Beware, that
for technical reasons, NO periodic boundary conditions can be used. A cut-off
value may be specified to reduce the effort in pure MM electrostatic
interaction computations. A sensible value should at least include the size of
the QM system. Note that this will affect only the MM energy and gradients.
Regardless of this value, ALL electrostatic interactions will be handled by
the QM program. 

For explicit solvent computations we strongly recommend to cut out a
spherically shaped solvent droplet around the system of interest after
selecting a snapshot out of a periodic boundary MM simulation. This can be
easily done using Amber's ambmask utility:

>> ambmask -p tym.top -c real.crd -prnlev 0 -out pdb -find '@11<:12' > sphere.pdb

where 
-p file.top is the Amber topology file
-c geom.crd is the file with the Cartesian coordinates (Amber format) of the snapshot 
-prnlev is the amount of input printed (with 0 it just prints the coordinates ready 
for being read with tleap)
-find '@11<:12' means all atoms within a radius of 12A form atom 11. by putting @ 
you select an atom and with two points (:) a residue.

To avoid undesired side-effects one should process the .pdb through Amber's tleap:
a) to generate a .pdb with the proper residue numbering;
b) to generate a .top and .crd files since the number of molecules has changed from 
the original snapshot after cutting out the spherical solvent.

>> tleap
load $FORCE_FIELDS
loadoff $NON-STANDARD_LIBRARIES
loadamberparams $NON-STANDARD_PARAMETERS
geom = loadpdb geom.pdb
ADDITIONAL LINES (LIKE EXPLICITE S-S BOND DEFINITIONS)
saveamberparm geom geom.top geom.crd
savepdb geom geom.pdb

Replace the $VARIABLES with your corresponding values for force field,
libraries etc. The obtained .pdb, .crd and .top files can then be further processed 
through the Prepare_cobramm_input.pl script to generate the starting files for 
performing COBRAMM geometry optimizations and MD (see Manual for details)

The above !sander ?sander input can be also used for more
advanced instructions to Amber which would allow to optimize/equilibrate the
low layer for several steps after every QM computation. Thus, also the
"frozen" low layer can be allowed to move.

** Note that in amber12 the structure of the .top file has slightly
changed with respect to earlier versions. This may create problems
when cobram tries to read / modify charges, and the calculation will
then stop. We have mostly accounted for this, however, if you still
experience problems, you may be able to shift the fields in real.top
yourself as a temporary workaround.  COBRAM expects the field 'MASS'
(ver <12) or 'ATOMIC_NUMBER' (ver >=12) to appear directly after the
field 'CHARGE', carefully resorting the fields into this order may
help.
