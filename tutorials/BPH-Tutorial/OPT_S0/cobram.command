!keyword
type=optxg qm-type=gauss stepsize=10 qmem=1000MB nproc=8 nsteps=1000
?keyword

!sander 
Comment
 &cntrl
  imin   = 1,
  maxcyc = 0,
  ntb    = 0,
  igb    = 0,
  ntr    = 0,
  ibelly = 1,
  cut    = 14,
  ntxo   = 1
 /
?sander 

!gaussian
#P CAM-B3LYP/6-31G* NOSYM

Optimization of BPH in water

0  1
?gaussian

