!keyword
type=mdv qm-type=gauss stepsize=10 qmem=1000MB nproc=8 tstep=0.5 nsteps=1000
?keyword

!sander 
Comment
 &cntrl
  imin   = 1,
  maxcyc = 0,
  ntb    = 0,
  igb    = 0,
  ntr    = 0,
  ibelly = 1,
  cut    = 14,
  ntxo   = 1
 /
?sander 

!gaussian
#P CAM-B3LYP/6-31G* NOSYM TD(root=3,singlets)

MD of BPH in S3 in water

0  1
?gaussian

!gaussadd
--link1--
%nproc=8
%mem=1000MB
%chk=gaussian-QM.chk
#P Guess=Check geom=Check CAM-B3LYP/6-31G* NOSYM TD(root=3,triplets) charge=Check

MD of BPH triplet states

0  1
?gaussadd


