
# Tutorial #1 - Ground/excited state optimization and MD of benzophenone in H2O

This tutorial will cover MM and QM/MM setup of water solvated
*benzophenone* (BPH) to compute absorption and emission properties
with *TD-CAM-B3LYP* and *Gaussian* and finally excited state 
molecular dynamics. 

Note that this tutorial just exemplifies the usage 
of COBRAMM, it is not meant for obtaining publishable results.

The files for this tutorial are in the subdirectories:
* ``MM-setup`` setup of *AMBER* input files for BPH
  in water
* ``equil`` equilibration of the solvent at 300 K and
  1 bar with *AMBER*
* ``QMMM-setup`` setup of *COBRAMM* input files with 
  [Prepare_cobramm_input.pl](https://gitlab.com/cobrammgroup/prepare_cobramm_input)
* ``OPT_S0`` QM/MM geometry optimizations
* ``S3_MD`` QM/MM molecular dynamics
 
Instructions for this tutorial are available from the project 
[wiki pages](https://gitlab.com/cobrammgroup/cobramm/-/wikis/Tutorial-1).

