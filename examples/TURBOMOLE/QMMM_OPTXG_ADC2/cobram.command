!keyword
type=optxg qm-type=turbo qmem=2000MB rigrad=yes
?keyword

!sander
Torsion 1Bu
 &cntrl
  imin   = 1,
  maxcyc = 0,
  ntb    = 0,
  igb    = 0,
  ntr    = 0,
  ibelly = 1,
  cut    = 12,
  ntxo   = 1
 /
?sander

!optxg
#P OPT=(nolinear,nomicro) iop(1/9=1,1/8=3)
?optxg


