!keyword
type=irc qm-type=molpro nsteps=100 ircpoints=5 ircstepsize=10 qmem=200MW basis=STO-3G sstep=-1
?keyword


!molpro
{multi;
occ,9;
closed,6;
wf,16,1,0;
state,3
CPMCSCF,GRAD,2.1,record=5100.1;}
?molpro


!singlepoint
{multi;
occ,9;
closed,6;
wf,16,1,0;
state,3}

{rs2c;
state,1,1;
}

{rs2c;
state,1,2;
}

{rs2c;
state,1,3;
}
?singlepoint

