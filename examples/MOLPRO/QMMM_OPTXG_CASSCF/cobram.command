!keyword
type=optx qm-type=molpro nsteps=100 nproc=1 qmem=1000MB basis=STO-3G
?keyword

!sander 
MD Pentadien
&cntrl
imin   = 1,
maxcyc = 0,
ntb    = 0,
igb    = 0,
ntr    = 0,
ibelly = 1,
cut    = 9,
ntxo   = 1
/
?sander

!molpro
{multi;
occ,40;
closed,38;
wf,78,1,0;
state,3
CPMCSCF,GRAD,3.1,record=5100.1;
}
?molpro

