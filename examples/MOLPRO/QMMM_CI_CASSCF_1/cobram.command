!keyword
type=ci qm-type=molpro nsteps=1 nrpoc=1 qmem=1000MB basis=STO-3G
?keyword

!sander
MD Pentadien
&cntrl
imin   = 1,
maxcyc = 0,
ntb    = 0,
igb    = 0,
ntr    = 0,
ibelly = 1,
cut    = 9,
ntxo   = 1
/
?sander

!molpro
{multi;
occ,40;
closed,38;
wf,78,1,0;
state,3;
CPMCSCF,NACM,1.1,2.1,record=5100.1;
CPMCSCF,GRAD,1.1,record=5101.1;
CPMCSCF,GRAD,2.1,record=5102.1;}

{Force;
samc,5100.1;
conical,6100.1;
}
{Force;
samc,5101.1;
conical,6100.1;
}
{Force;
samc,5102.1;
conical,6100.1;
}
?molpro

