!keyword
type=ci qm-type=molrpo nsteps=1 qmem=1000MB basis=STO-3G
?keyword

!sander
MD Pentadien
&cntrl
imin   = 1,
maxcyc = 0,
ntb    = 0,
igb    = 0,
ntr    = 0,
ibelly = 1,
cut    = 9,
ntxo   = 1
/
?sander

!molpro
{multi;
occ,40;
closed,38;
wf,78,1,0;
state,3;
CPMCSCF,NACM,1.1,2.1,record=5100.1;
}
{Force;
samc,5100.1;
}
{rs2;
state,1,1;
noexc;
}
{force
}
{rs2;
state,1,2;
noexc;
}
{force
}
?molpro

