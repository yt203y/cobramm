!kyeword
type=irc qm-type=molpro nsteps=100 ircpoints=5 ircstepsize=10 qmem=1000MB basis=STO-3G
?kyeword

!sander
MD Pentadien
&cntrl
imin   = 1,
maxcyc = 0,
ntb    = 0,
igb    = 0,
ntr    = 0,
ibelly = 1,
cut    = 9,
ntxo   = 1
/
?sander

!molpro
{multi;
occ,40;
closed,38;
wf,78,1,0;
state,3
CPMCSCF,GRAD,2.1,record=5100.1;
}
?molpro

