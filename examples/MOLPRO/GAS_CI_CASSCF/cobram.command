!keyword
type=ci qm-type=molpro qmem=200MW basis=STO-3G nsteps=1
?keyword

!molpro
{multi;
occ,9;
closed,6;
wf,16,1,0;
state,3
CPMCSCF,NACM,1.1,2.1,record=5100.1;
CPMCSCF,GRAD,1.1,record=5101.1;
CPMCSCF,GRAD,2.1,record=5102.1;}

{Force;
samc,5100.1;
conical,6100.1;
}
{Force;
samc,5101.1;
conical,6100.1;
}
{Force;
samc,5102.1;
conical,6100.1;
}
?molpro

