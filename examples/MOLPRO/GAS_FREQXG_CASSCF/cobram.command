!keyword
type=freqxg qm-type=molpro nsteps=1000 qmem=200MW basis=STO-3G
?keyword

!molpro
{multi;
occ,9;
closed,6;
wf,16,1,0;
state,3
CPMCSCF,GRAD,2.1,record=5100.1;
}
?molpro

