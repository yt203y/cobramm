!keyword
type=optxg qm-type=gauss nsteps=100 qmem=1000MB nproc=8 geomem=1000MB
?keyword

!sander
MD Pentadien
&cntrl
imin   = 1,
maxcyc = 0,
ntb    = 0,
igb    = 0,
ntr    = 0,
ibelly = 1,
cut    = 9,
ntxo   = 1
/
?sander

!gaussian
#p CAM-B3LYP/sto-3g TD(NStates=5,Root=3) nosym

comment line

0 1
?gaussian

