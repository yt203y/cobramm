!keyword
type=mdv qm-type=gauss qmem=500MB tstep=0.5 nsteps=100
?keyword


!sander
MD Pentadien
 &cntrl
  imin   = 1,
  maxcyc = 0,
  ntb    = 0,
  igb    = 0,
  ntr    = 0,
  ibelly = 1,
  cut    = 12,
  ntxo   = 1
 /
?sander


!gaussian
#P TD(NSTATES=5,root=3) CAM-B3LYP/sto-3g NOSYM

0  1

?gaussian

