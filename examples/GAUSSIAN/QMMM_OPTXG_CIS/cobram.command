!keyword
type=optxg qm-type=gauss nproc=4 qmem=500MB geomem=500MB
?keyword

!sander
minimizzazione
&cntrl
imin   = 1,
maxcyc = 2,
ntb    = 0,
igb    = 0,
ntr    = 0,
ibelly = 1,
cut    = 12,
ntxo   = 1
/
?sander

!gaussian
#P CIS(NSTATES=5,root=3) NOSYM

0  1
?gaussian

