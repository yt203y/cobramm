!keyword
type=optxg qm-type=gauss nsteps=100 qmem=1000MB nproc=1 geomem=1000MB
?keyword

!sander
MD Pentadien
&cntrl
imin   = 1,
maxcyc = 0,
ntb    = 0,
igb    = 0,
ntr    = 0,
ibelly = 1,
cut    = 9,
ntxo   = 1
/
?sander

!gaussian
#p casscf(4,3,nroot=3,stateaverage)/sto-3g nosym 

SA-3-CASSCF optimization with equal roots with approximate computation of the CPMCSCF eqs.

0 1
?gaussian

!gaussadd
0.333333330.333333330.33333333
?gaussadd
