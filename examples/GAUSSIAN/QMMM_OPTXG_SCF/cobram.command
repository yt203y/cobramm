!keyword
type=optxg qm-type=gauss nsteps=100 qmem=500MB nproc=4 geomem=500MB
?keyword

!sander
minimizzazione
&cntrl
imin   = 1,
maxcyc = 2,
ntb    = 0,
igb    = 0,
ntr    = 0,
ibelly = 1,
cut    = 12,
ntxo   = 1
/
?sander

!gaussian
#P hf/sto-3g nosym 

0  1
?gaussian

