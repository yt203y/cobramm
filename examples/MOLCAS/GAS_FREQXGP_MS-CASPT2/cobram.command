!keyword
type=freqxgp qm-type=molcas numproc=16 ricd=yes nsteps=1000 basis=STO-3G
?keyword

!molcas
&RASSCF
LUMORB
SYMMETRY
1
NACTEL
4 0 0
INACTIVE
6
ras2
3
CIROOT
3 3 1
MAXORB
1
RLXROOT
2

&CASPT2
multistate
3 1 2 3
PROP
ipea
0.0
imag
0.2

&ALASKA
NUME
DELTA
0.001889
?molcas


