!keyword
type=optxgp qm-type=molcas nsteps=100 numproc=12 stepsize=5 qmeme=1000MB ricd=yes basis=STO-3G
?keyword

!sander
MD Pentadien
&cntrl
imin   = 1,
maxcyc = 0,
ntb    = 0,
igb    = 0,
ntr    = 0,
ibelly = 1,
cut    = 9,
ntxo   = 1
/
?sander

!molcas
&RASSCF
LUMORB
SYMMETRY
1
NACTEL
2 2 2
INACTIVE
38
ras1
2
ras2
2
ras3
2
CIROOT
3 3 1
MAXORB
1
RLXRoot
3
End of Input

&CASPT2
multistate
3 1 2 3
PROP
?molcas

