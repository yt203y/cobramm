!keyword
type=optxg qm-type=molcas qmem=1000MB ricd=yes basis=STO-3G nsteps=100
?keyword

!molcas
&RASSCF
LUMORB
SYMMETRY
1
NACTEL
4 0 0
INACTIVE
6
ras2
3
CIROOT
3 3 1
MAXORB
1
RLXRoot
2
End of Input

