!keyword
type=ci qm-type=molcas nacs=yes stepsize=5 ricd=no qmem=1000MB basis=STO-3G
?keyword


######################################################################################
# comments
######################################################################################
# nacs=yes	      compute NACs through Molpro (works for CASSCF only, no RASSCF)
# stepsize=5	      maxstep for the Gaussian optimizer
# ricd=no	      no RICD (do not switch on with Molpro NACs
# basis= STO-3G       basis set for Molcas (ANO basis sets not recognized in Molpro)
######################################################################################


!sander
MD Pentadien
&cntrl
imin   = 1,
maxcyc = 0,
ntb    = 0,
igb    = 0,
ntr    = 0,
ibelly = 1,
cut    = 9,
ntxo   = 1
/
?sander

!molcas
&RASSCF
LUMORB
SYMMETRY
1
NACTEL
2 0 0
INACTIVE
38
ras2
2
CIROOT
3 3 1
MAXORB
1
RLXRoot
2
End of Input
?molcas



