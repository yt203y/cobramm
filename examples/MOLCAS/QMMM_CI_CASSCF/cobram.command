!keyword
type=ci qm-type=molcas nsteps=1 stepsize=5 qmem=1000MB basis=STO-3G
?Keyword

!sander
MD Pentadien
&cntrl
imin   = 1,
maxcyc = 0,
ntb    = 0,
igb    = 0,
ntr    = 0,
ibelly = 1,
cut    = 9,
ntxo   = 1
/
?sander

!molcas
&RASSCF
LUMORB
SYMMETRY
1
NACTEL
2 0 0
INACTIVE
38
ras2
2
CIROOT
3 3 1
MAXORB
1
RLXRoot
2
End of Input
?molcas



