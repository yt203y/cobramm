!keyword
type=mdv qm-type=molcas qmem=1000MB tstep=0.5 tsshort=0.25 surhop=persico ediff=30.0 basis=STO-3G
?keyword


!molcas
&RASSCF
LUMORB
SYMMETRY
1
NACTEL
4 0 0
INACTIVE
6
ras2
3
CIROOT
3 3 1
MAXORB
1
RLXROOT
2
End of Input
?molcas

