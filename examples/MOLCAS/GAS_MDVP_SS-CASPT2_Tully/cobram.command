!keyword
type=mdvp qm-type=molcas nsteps=500 qmem=1000MB tstep=0.5 tsshort=0.25 surhop=persico ediff=30.0 basis=STO-3G numproc=12
?keyword

###################################################################
# comments:
###################################################################
# type=mdv           parallel numerics for CASPTS gradients
# surhop=persico     use tully surface hopping + decoherence corr.
# tstep=0.5	     use 0.5 fs for normal time step
# tsshort=0.25	     use 0.25 fs close to CI
# ediff=30.0	     activate smaller timestep at state energy 
  		     difference of 30 kcal/mol
###################################################################

!molcas
&RASSCF
LUMORB
SYMMETRY
1
NACTEL
4 0 0
INACTIVE
6
ras2
3
CIROOT
3 3 1
THRS
1.0e-07 1.0e-02 1.0e-02
MAXORB
1
RLXROOT
1
End of Input

&CASPT2
multistate
3 1 2 3
NOMULT
PROP
ipea
0.0
imag
0.2
?molcas

