!keyword
type=freqxgp qm-type=molcas qmem=5000MB geomem=2000MB numproc=12 nsteps=1000 ricd=yes basis=STO-3G
?keyword


!sander 
MD Pentadien
&cntrl
imin   = 1,
maxcyc = 0,
ntb    = 0,
igb    = 0,
ntr    = 0,
ibelly = 1,
cut    = 9
/
?sander

!molcas
&SCF
&MBPT2
?molcas

